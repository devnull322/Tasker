#include "libs/tasker/tasker.h"
#include <ncurses.h>


#define UNDONE "( )"
#define DONE   "(X)"
#define SPACE  ' '
#define Y_CUR getcury(stdscr)
#define X_CUR getcurx(stdscr)
#define X_MAX getmaxx(stdscr)
#define Y_MAX getmaxy(stdscr)
#define showtitle() mvaddstr(0, X_MAX/2 - 6, "Tasker")

//#define showsubtitles()\clearline(Y_MAX - 1);\(0, (X_MAX/2) - 6, )

#define showtasksfrom(pos) \
int counter = 0;\
for (Task* cur = notebook; cur != NULL; cur = cur->next) {\
	counter++;\
	if ( Y_CUR == Y_MAX - 2)\
		break;\
	if (counter >= pos)\
		mvprintw( Y_CUR + 1, 1, cur->done? DONE" %s" : UNDONE" %s", cur->name);\
}



int main(void) {
	Task* notebook = NULL;


	appendtask(&notebook, "test 1", false);
	appendtask(&notebook, "test 2", false);
	appendtask(&notebook, "test 3", true);
	appendtask(&notebook, "test 4", false);
	appendtask(&notebook, "test 5", false);
	appendtask(&notebook, "test 6", true);
	appendtask(&notebook, "test 7", false);
	appendtask(&notebook, "test 8", false);
	appendtask(&notebook, "test 9", false);
	appendtask(&notebook, "test 10", false);
	appendtask(&notebook, "test 11", true);
	appendtask(&notebook, "test 12", false);
	appendtask(&notebook, "test 13", true);
	appendtask(&notebook, "test 14", false);
	appendtask(&notebook, "test 15", true);
	appendtask(&notebook, "test 16", false);
	

	initscr();  noecho();  cbreak();  scrollok(stdscr, true);  keypad(stdscr, true);
	unsigned int y = 1, task_at_top = 1,  cursor = 1;


	while (true) {
		clear();
		showtitle();
		showtasksfrom(task_at_top);
		move(y, 2);


		switch ( getch() ) {

			case KEY_DOWN:
				if (cursor == counttasks(notebook)) break;
				cursor++;
				if (Y_CUR == Y_MAX - 2) {
					task_at_top++;
					break;
				}
				move(Y_CUR + 1, 2);
				break;


			case KEY_UP:
				if (cursor == 1) break;
				cursor--;
				if (Y_CUR == 1) {
					task_at_top--;
					break;
				}
				move(Y_CUR - 1, 2);
				break;


			case SPACE:
				toggletask(notebook, cursor);
				break;


			case 'q':
				endwin();
				freealltasks(notebook);
				return 0;
		}

		y = Y_CUR;
		refresh();
	}
}

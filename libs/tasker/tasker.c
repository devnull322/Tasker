#include "tasker.h"


Task* newtask(char *name, bool done) {
    
	Task *newTask = malloc( sizeof(Task) );
    
	newTask->name = malloc( strlen(name) + 1 );
    strncpy( newTask->name, name, strlen(name) );
    newTask->name[ strlen(name) ] = '\0';
    
	newTask->done = done;
    
	return newTask;
}

void appendtask(Task** t, char* name, bool done) {
	if (*t == NULL) {
		*t = newtask(name, done);
		return;
    }

    for (Task* cursor = *t;  ; cursor = cursor->next) {
		if (cursor->next == NULL) {
			cursor->next = newtask(name, done);
			break;
		}
    }
}

int counttasks(Task* t) {
	int counter = 0;
	for (Task* cursor = t; cursor != NULL; cursor = cursor->next)
		counter++;

	return counter;
}

Task* gettask(Task* t, int pos) {
	int counter = 0;
	for (Task* cursor = t; cursor != NULL; cursor = cursor->next) {
		counter++;
		if (counter == pos)
			return cursor;
	}
	return NULL;
}

void deletetask(Task** t, int pos) {
	if (pos < 1)
		return;
	
	if (pos == 1) {
		Task* tmp = (*t)->next;
		free((*t)->name);
		free(*t);
		*t = tmp;
		return;
	}

	int counter = 0;
	for (Task* cursor = *t; cursor != NULL; cursor = cursor->next) {
		counter++;
		if (counter == pos - 1) {
			Task* tmp = cursor->next->next;
			free(cursor->next->name);
			free(cursor->next);
			cursor->next = tmp;
			break;
		}
	}
}


void toggletask(Task* t, int pos) {
	if (pos < 1)
		return;

	int counter = 0;
	for (Task* cursor = t; cursor != NULL; cursor = cursor->next) {
		counter++;
		if (counter == pos) {
			cursor->done = !cursor->done;
			return;
		}
	}
}


void freealltasks(Task* t) {

    for ( Task* cursor = t; cursor != NULL; ) {
		Task *tmp = cursor->next;
		free(cursor->name);
		free(cursor);
		cursor = tmp;
    }
}

#ifndef TASKER_H
#define TASKER_H


#include <stdbool.h>
#include <string.h>
#include <stdlib.h>


typedef struct _Task {
	char* name;
	bool done;
	struct _Task* next;
} Task;


extern Task* newtask(char *name, bool done);


extern void appendtask(Task** t, char* name, bool done);


extern int counttasks(Task* t);


extern Task* gettask(Task* t, int pos);


extern void deletetask(Task** t, int pos);


extern void toggletask(Task* t, int pos);


extern void freealltasks(Task* t);


#endif

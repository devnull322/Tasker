#ifndef FILEOPS_H
#define FILEOPS_H
#include <stdio.h>

extern char freadc(FILE* f);
extern unsigned long fnextc(FILE* f, const char c);
extern unsigned long fprevc(FILE* f, const char c);

#endif

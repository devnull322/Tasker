#include "fileops.h"


char freadc(FILE* f){
	char c = fgetc(f);
	fseek(f, SEEK_SET, ftell(f) - 1);
	return c;
}


unsigned long fnextc(FILE* f, const char c) {

	unsigned long counter = 0;
	unsigned long f_original_pos = ftell(f);
	fseek(f, SEEK_SET, f_original_pos + 1);

	while ((fgetc(f)) == c ) {
		if (freadc(f) != EOF) {
			counter = 0;
			break;
		}
		counter++;
	}
	return counter;
}


unsigned long fprevc(FILE* f, const char c) {

	unsigned long counter = 1;
	unsigned long f_original_pos = ftell(f);
	fseek(f, SEEK_SET, f_original_pos - 1);

	while (freadc(f) != c || ftell(f) == 0)
		fseek(f, SEEK_SET, f_original_pos - (--counter));

	return counter - 1;
}


/*
 *  char* readline(FILE* f) {
 *  	
 *  }
 */

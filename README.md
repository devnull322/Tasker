# Tasker (TASK managER)

## requirements:

- ncurses library
- GNU GCC
- GNU Make

## how to install:

1. Download the source code:
    git clone https://codeberg.org/gabrielovskyy/Tasker

2. Go to the downloaded directory and compile the source code:
    make install

Now, you can execute it with:
    ./taskerbin

If you want to unninstall the program, just type:
    make clean
then, press Enter.
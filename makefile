install: tasker
	$(CC) main.c -Werror -Wall -g -lncurses -Llibs/bin/ -ltasker -o taskerbin

tasker:
	@if [ ! -d libs/obj ] && [ ! -d libs/bin ]; then\
		mkdir libs/obj libs/bin;\
	fi
	
	$(CC) libs/tasker/tasker.c -c -Wall -Werror -fpic -o libs/obj/tasker.o
	ar rcs libs/bin/libtasker.a libs/obj/tasker.o

clean:
	rm -rf libs/bin
	rm -rf libs/obj
	rm taskerbin
